# ROSDISTRO

You're on a fork from <github.com/ros/rosdistro.git> on default branch `dm-main`.
Its aim is the customization of `rosdep/base.yaml` to add missing keys I can spot during
a `rosdep install` atempt. Updates against the original repo are not
automatized (since rosdep can check several YAML files from several sources).
The default branch `master` from original repo is kept as-is to facilitate updates
from original and merge/pull requests towards original.

If `rosdep install` complains about missing dependencies, you can do the same
by learning [how to do it](https://docs.ros.org/en/independent/api/rosdep/html/contributing_rules.html).

Cheers. Sylvain

## Main README.md

This repo maintains the lists of repositories defining ROS distributions.

It is the implementation of [REP 143](http://ros.org/reps/rep-0143.html).

It also the home of the rosdep rules.

Guide to Contributing
---------------------

Please see [CONTRIBUTING.md](CONTRIBUTING.md).

Review guidelines
-----------------

Please see the [review guidelines](REVIEW_GUIDELINES.md) to look at the criteria to get a pull request merged into this repository.
